$Id: README.txt,v 1.3 2009/06/18 13:03:46 rolfmeijer Exp $

I use this theme in two seperate ways.
First, I use it as a starting point for new themes. So I don’t have to empty the stylsesheet for every theme I start.
Second, it is a reference so I can see what has been styled by core and the different modules. For instance the tabs have an outline and some background color. If I want to change that – using only CSS – I need to overwrite that.

Install it and make a subtheme for your own custom theme. See http://drupal.org/node/441088 for more on subtheming.

==================================================

Author: Rolf Meijer for Studio Karu <rolf@karu.nl>

Studio Karu
Thorbeckestraat 50
8121 ZD  Olst
The Netherlands

http://karu.nl/
